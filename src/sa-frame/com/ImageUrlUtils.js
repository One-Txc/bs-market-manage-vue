

export default class ImageUrlUtils {

  static transformUrl (url) {
    if (url == null || url == undefined) {
      return url
    }
    // http://127.0.0.1:8099/upload/image/2023/03-16/1678953546905758081047.jpg
    if (url.startsWith('http://127.0.0.1:8099')) {
      return url.replace('http://127.0.0.1:8099', process.env.VUE_APP_BASE_API)
    }
    return url
  }
}
